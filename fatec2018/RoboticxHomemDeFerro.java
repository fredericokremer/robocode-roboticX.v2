/**
 * Copyright (c) 2001-2018 Mathew A. Nelson and Robocode contributors
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://robocode.sourceforge.net/license/epl-v10.html
 */
package fatec2018; // Nome padrão de pacote, decido pelo Professor Ângelo Luz.
    
import robocode.*;
import java.awt.Color;

    /** Declaração da classe.
        RoboticxHomemDeFerro - robô fatec2018 By Edson Fagundes Garcia.
        Este robô tem movimentação em forma de quadrado arredondado.
    */ 
public class RoboticxHomemDeFerro extends AdvancedRobot {

    /** Variáveis declaradas.
    *   turnValue - variável que controla o giro do radar.
    *   angle - variável que controla o ângulo do robô.
    *   posiçãox - variável que controla a posição no eixo horizontal do robô.
    *   posiçãoy - variável que controla a posição no eixo vertical do robô.
    */
    double turnValue; 
    double angle;      
    double posicaox;  
    double posicaoy;  
                     
     
    /** O método run é invocado ao começar o programa. 
        Ele fica em constante execução.
    */
    public void run() {

        turnValue = 1440; 

        /** Este método e invocado dentro do método run para ao iniciar a partida
            colocar o robô em uma posição específica da arena.
        */ 
        posicionar();

        /** Métodos usados para modificação de cores de partes do robô, 
            respectivamente: corpo, canhão, radar, escaneamento e tiro.
        */
        setBodyColor(Color.RED);
        setGunColor(Color.getHSBColor(238, 173, 14));
        setRadarColor(Color.BLACK);
        setScanColor(Color.RED);
        setBulletColor(Color.white);
       
       
        /** Loop de movimentação do robô, roda sempre até que o robô 
            seja reiniciado ou eliminado.
        */
        while (true) {

            setTurnGunLeft(turnValue); // movimentação do canhão para a esquerda sem alterar outro movimento que esteja sendo feito pelo robô.
            ahead(500);                // movimentação do robô para frente. 
			scan();
            setTurnLeft(90);           // movimentação do robô para esquerda sem alterar outro movimento que esteja sendo feito pelo robô. 
			scan();
			execute();
        } 
        
    }

       /** Método void posicionar.
           Esse método foi criado para posicionar o robô na área de batalha
           sempre que ele for eliminado ou reinializado. 
       */
    public void posicionar() { 
           
            angle = 360 - getHeading(); // variável de controle do angulo do robô.
            turnRight(angle);           // movimentação para o lado direito.
            setTurnGunLeft(turnValue);  
            posicaoy = (getBattleFieldHeight() - getY() - 210); // armazena a um valor double equivalente a posição no eixo y do robô dentro da variável posiçãoy.
            ahead(posicaoy);               
            turnLeft(90);               // movimentação para o lado esquerdo, angulo de 90 graus.
            setTurnGunLeft(turnValue);  
            posicaox = (getX() - 210);  // armazena a um valor double equivalente a posição no eixo x do robô dentro da variável posiçãox.
            ahead(posicaox);
            setTurnRight(turnValue);
            turnLeft(90);
            
    }

    /** Método onScannedRobot
        Este método é invocado toda vez que escaneado um robô adversário ou até mesmo amigo.
        Decide a ação a ser feita após o escanamento.
        Neste método o robô vai abrir fogo contra o inimigo.
        @ param escaneado - o que foi escaneado.
    */
    public void onScannedRobot(ScannedRobotEvent escaneado) {

        String identificacao = escaneado.getName(); // variável que vai armazenar o nome do robô escaneado na arena.

        /** Aqui foi feita uma condição para que somente o robô use seu canhão 
            caso o robô escaneado nao seja de sua equipe.
        */
        if ((identificacao.startsWith("fatec2018.Roboticx") || identificacao.equals("samplesentry.BorderGuard"))){
			return;
		}

		else {

        /** Método getDistance.
            Este método retorna a distance em pixels do robô escaneado.
            Foi usado neste caso uma condição para tiros mais fortes com menos velocidade se a distância for pequena, e tiros mais fracos 
            com porem mais rapidos se a distância for grande.
        */    
          if (escaneado.getDistance() < 100) {
            fire(5); // tiro mais forte e lento.
          } else {
            fire(2); // tiro mais fraco e rápido.
          };
		  scan();
        }
        
    }

    /** Método onHitByBullet
        Este método é invocado toda vez que o robô é alvejado por algum tiro.
        Decide a ação a ser feita após ser acertado.
        @ param event - local da arena. O que vai ser feito.
    */

    public void onHitByBullet(HitByBulletEvent event) {
            
            
    }

    /** Método onHitWall
        Este método é invocado toda vez que o robô atingi a parede da arena.
        Decide a ação a ser feita após a colisão.
        @ param event - local da arena. O que vai ser feito.
    */
    public  void onHitWall(HitWallEvent event) {

            turnLeft(120); // gira 120 graus
			scan();
            ahead(100);
			scan();

    }

    /** Método onHitRobot
        Este método é invocado toda vez que houver uma colisão com outro robô.
        Decide a ação a ser feita após a colisão.
        @ param event - local da arena. O que vai ser feito.
    */ 
    public void onHitRobot(HitRobotEvent event) {

            back(70);
            posicionar();
            ahead(500);
            setTurnGunLeft(turnValue);
            setTurnLeft(90);   
        
    }

}  
